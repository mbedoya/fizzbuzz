"use strict";

(function () {

    // ... General functions

    const simpleContainer = document.getElementById("simple");
    const arrayContainer = document.getElementById("array");

    const timer = function () {
        const initialTime = new Date().getTime();
        return function () {
            return new Date().getTime() - initialTime;
        }
    }

    const print = function (value) {
        //console.log(value);
    }

    const timeLoggable = function (fx, name) {
        name = name || '';
        return function () {
            const timeCalc = timer();
            fx(arguments);
            console.log(name + ' took ' + timeCalc() + " ms");
        }
    }

    const start = 1;
    const end = 100000;

    // ... Simple solution

    const simpleFB = function (args) {
        for (let i = args[0]; i < args[1]; i++) {
            let output = '';
            if (i % 3 === 0) {
                output += "Fizz";
            }
            if (i % 5 === 0) {
                output += "Buzz";
            }
            print(output ? output : i);
        }
    };

    // ... Generating array from numbers

    const simpleExecution = timeLoggable(simpleFB, "Simple Solution");
    simpleExecution(start, end);
    simpleContainer.innerHTML = simpleFB.toString();

    const arrayBaseFB = function (args) {
        const elements = Array.from({ length: args[1] - args[0] }, (e, i) => args[0] + i);

        elements.forEach(function (e) {
            let output = "";
            if (e % 3 === 0) {
                output += "Fizz";
            }
            if (e % 5 === 0) {
                output += "Buzz";
            }
            print(output ? output : e);
        });
    };

    const arrayBasedExecution = timeLoggable(arrayBaseFB, "Array Based");
    arrayBasedExecution(start, end);
    arrayContainer.innerHTML = arrayBaseFB.toString();

    // ... more expressions, less statements

    const convertElement = e => {
        let output = "";
        if (e % 3 === 0) {
            output += "Fizz";
        }
        if (e % 5 === 0) {
            output += "Buzz";
        }
        return output ? output : e
    };

    const expressionFB = function (args) {
        const elements = Array.from({ length: args[1] - args[0] }, (e, i) => args[0] + i);

        elements
            .map(e => convertElement(e))
            .forEach( e => print(e));
    }

    const expBasedExecution = timeLoggable(expressionFB, "Expression Based");
    expBasedExecution(start, end);

    // ... Converting conditions to expressions

    const fizz = x => x%3 === 0 ? "Fizz" : "";
    const buzz = x => x%5 === 0 ? "Buzz" : "";
    const please = x => x%7 === 0 ? "Please" : "";
    const eight = x => x%8 === 0 ? "Eight" : "";

    const converter = (e, fxs, str = "") => {
        if (fxs.length === 1) {
            str += fxs[0](e);
            return str ? str : e;
        }

        return converter(e, fxs.slice(1), str + fxs[0](e));
    };
    
    const advExpressionFB = function (args) {
        const elements = Array.from({ length: args[1] - args[0] }, (e, i) => args[0] + i);    

        elements
            .map(e => converter(e, [fizz, buzz, please, eight]))
            .forEach(e => print(e));
    }

    const AdvExpBasedExecution = timeLoggable(advExpressionFB, "Adv Expression Based");
    AdvExpBasedExecution(start, end);

})();